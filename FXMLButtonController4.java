package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;


public class FXMLButtonController4 implements Initializable {
	@FXML
	private Label label;
	
	@FXML
	private void handleButtonAction (ActionEvent event){
		System.out.println("You clicked me!");
		label.setText("Bom dia!");
	}
	@FXML
	private Label label2;
	@FXML
	private void handleButtonAction2 (ActionEvent event){
		System.out.println("You clicked me!");
		label2.setText("Boa tarde!");
	}
	@FXML
	private Label label3;
	@FXML
	private void handleButtonAction3 (ActionEvent event){
		System.out.println("You clicked me!");
		label3.setText("Bom S�bado");
	}
	@FXML
	private Label label4;
	@FXML
	private void handleButtonAction4 (ActionEvent event){
		System.out.println("You clicked me!");
		label4.setText("Bom Domingo");
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg4){
	}
}
